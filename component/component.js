Vue.component('nav-item', {
  data: function () {
    return {
      count: 0
    }
  },
  template: `
  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">iBaby</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="index.html">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>
  <li class="nav-item ">
    <a class="nav-link" href="subsidy.html">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>幼兒補助資料</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

</ul>
  `
})

new Vue({ el: '#components-demo' })
